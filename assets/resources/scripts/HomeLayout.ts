// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"

const { ccclass, property } = cc._decorator

@ccclass
export default class HomeLayout extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    onPlay() {
        this.node.active = false
        cc.Canvas.instance.node.getChildByName('GameLayout').active = true
    }

    onHowTo() {
        this.node.active = false
        cc.Canvas.instance.node.getChildByName('HowToLayout').active = true
    }

    // onLoad () {}

    start() {

    }

    // update (dt) {}
}
