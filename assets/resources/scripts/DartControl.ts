// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import NinjaControl from "./NinjaControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class DartControl extends cc.Component {
    gameLayout: GameLayout = null
    ninjaControl: NinjaControl = null

    // LIFE-CYCLE CALLBACKS:
    onCollisionEnter(other, self) {
        if (other.tag === 1) {
            // cc.log('Game over')
            this.ninjaControl.playDeath()
        } else if (other.tag === 2) {
            this.gameLayout.gameScore++
            this.gameLayout.lblScore.string = this.gameLayout.gameScore.toString()
        }
    }
    onLoad() {
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
        this.ninjaControl = this.node.getParent().getParent().getChildByName('Ninja').getComponent('NinjaControl')
    }

    start() {

    }

    // update (dt) {}
}
