// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"


const { ccclass, property } = cc._decorator

@ccclass
export default class GameOverLayout extends cc.Component {
    gameScore: number = 0
    gameLayout: GameLayout = null
    isNight: boolean

    @property(cc.Label)
    lblScore: cc.Label = null

    @property(cc.Node)
    bg: cc.Node
    // LIFE-CYCLE CALLBACKS:


    update(dt) {
        this.lblScore.string = this.gameScore.toString()
        if (this.isNight) {
            this.bg.getChildByName('Night').active = true
            this.bg.getChildByName('Day').active = false
        } else
            this.bg.getChildByName('Night').active = false
        this.bg.getChildByName('Day').active = true

    }
}
