// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import NinjaControl from "./NinjaControl"

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    isNight: number = 0
    gameScore: number = 0
    dart: cc.Node[] = []
    gameOverLayout = null
    ninjaControl: NinjaControl = null
    cooldown: number = 2
    grassCd: number = 4

    @property(cc.Label)
    lblScore: cc.Label = null

    @property(cc.Sprite)
    spBg: cc.Sprite[] = [null, null]

    @property(cc.Prefab)
    dartPrefab: cc.Prefab = null

    @property(cc.Prefab)
    grassPrefab: cc.Prefab = null

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        var collisionManager = cc.director.getCollisionManager()
        collisionManager.enabled = true

        this.gameScore = 0
        this.lblScore.string = this.gameScore.toString()
        this.gameStatus = GameStatus.Game_Playing
        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')
        this.ninjaControl = this.node.getChildByName('Ninja').getComponent('NinjaControl')
    }

    moveScreen(start: number, end: number) {
        for (let i = start;i < end;i++) {
            this.spBg[i].node.x -= 2
            if (this.spBg[i].node.x <= -1280) {
                this.spBg[i].node.x = 1280
            }
        }
    }

    onPlay(isReplay: boolean = true) {
        this.isNight++
        if (this.isNight === 3) {
            this.isNight = 1
        }

        this.gameScore = 0
        this.lblScore.string = this.gameScore.toString()
        this.gameStatus = GameStatus.Game_Playing
        this.ninjaControl.onReset()
        this.onSpawn()
    }

    gameOver() {
        this.gameStatus = GameStatus.Game_Over
        cc.Canvas.instance.node.getChildByName('GameLayout').active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        if (this.isNight != 2) {
            this.gameOverLayout.getComponent('GameOverLayout').isNight = false
        } else
            this.gameOverLayout.getComponent('GameOverLayout').isNight = true
    }

    onSpawn() {
        let dart = cc.instantiate(this.dartPrefab)
        this.node.getChildByName('Dart').addChild(dart)

        var minY = -210
        var maxY = -150
        dart.y = minY + Math.random() * (maxY - minY)

        let actionBy = cc.moveTo(2, cc.v2(-1400, dart.y))
        let destruction = cc.callFunc(() => {
            dart.destroy()
        }, this)
        let sequence = cc.sequence(actionBy, destruction)
        dart.runAction(sequence)
    }

    onSpawnGrass() {
        let grass = cc.instantiate(this.grassPrefab)
        this.node.getChildByName('Grass').addChild(grass)

        let actionBy = cc.moveTo(14, cc.v2(-1400, grass.y))
        let destruction = cc.callFunc(() => {
            grass.destroy()
        }, this)
        let sequence = cc.sequence(actionBy, destruction)
        grass.runAction(sequence)
    }

    update(dt) {
        if (this.gameStatus !== GameStatus.Game_Playing) {
            return
        }
        if (this.gameStatus === GameStatus.Game_Playing) {

            this.cooldown -= dt
            this.grassCd -= dt
            if (this.cooldown <= 0) {
                this.onSpawn()
                this.cooldown = Math.random() * (1.5 - 0.75) + 0.75
            }
            if (this.grassCd <= 0) {
                this.onSpawnGrass()
                this.grassCd = 5
            }
        }
        if (this.isNight != 2) {
            this.spBg[0].node.active = true
            this.spBg[1].node.active = true
            this.spBg[2].node.active = false
            this.spBg[3].node.active = false
            this.lblScore.node.color = new cc.Color(0, 0, 0)
            this.moveScreen(0, 2)
        }
        else {
            this.spBg[0].node.active = false
            this.spBg[1].node.active = false
            this.spBg[2].node.active = true
            this.spBg[3].node.active = true
            this.lblScore.node.color = new cc.Color(255, 255, 255)
            this.moveScreen(2, 4)
        }
    }

}
