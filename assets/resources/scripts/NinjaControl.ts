// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout, { GameStatus } from "./GameLayout"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class NinjaControl extends cc.Component {
    gameControl: GameScene = null
    gameLayout: GameLayout = null
    isDeath: boolean = false
    clickTime: any = new Date().getTime()
    isAvailable: boolean = true

    // @property(Number)
    jumpDuration: number = 0

    // @property(Number)
    jumpHeight: number = 0

    @property(cc.Animation)
    anim: cc.Animation = null

    // LIFE-CYCLE CALLBACKS:
    setJumpAction(jumpDuration, jumpHeight) {
        // jump up
        var jumpUp = cc.moveBy(jumpDuration, cc.v2(0, jumpHeight)).easing(cc.easeCubicActionOut())
        // jump down
        var jumpDown = cc.moveBy(jumpDuration, cc.v2(0, -jumpHeight)).easing(cc.easeCubicActionIn())
        // repeat
        return (cc.sequence(jumpUp, jumpDown))
    }

    setJumpDoubleAction(jumpDuration, jumpHeight) {
        // jump up
        var jumpUp = cc.moveBy(jumpDuration, cc.v2(0, jumpHeight)).easing(cc.easeCubicActionOut())
        // jump down
        var jumpDown = cc.moveBy(0.6, cc.v2(0, -jumpHeight)).easing(cc.easeCubicActionIn())
        // repeat
        return (cc.sequence(jumpUp, jumpDown))
    }

    playDeath() {
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
        // this.gameControl.audioSourceControl.stop()
        this.node.getParent().getChildByName('Dart').destroyAllChildren()
        this.node.getParent().getChildByName('Grass').destroyAllChildren()
        cc.Canvas.instance.node.getChildByName('GameLayout').stopAllActions()
        cc.Canvas.instance.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this)
        this.gameLayout.getComponent('GameLayout').gameStatus = GameStatus.Game_Over
        this.anim.play(this.anim.getClips()[1].name)
        this.node.runAction(cc.moveTo(1, this.node.x, this.node.y - 20))
        setTimeout(() => this.gameLayout.gameOver(), 1000)
    }

    onReset() {
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this)
        this.node.x = -337
        this.node.y = -170
        this.anim.play(this.anim.getClips()[0].name)
        this.isAvailable = true
    }

    onLoad() {
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
    }

    start() {

    }

    checkClick(firstClick, lastClick) {
        if (this.isAvailable) {
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Jump)
            if (lastClick - firstClick <= 300) {
                // double
                this.isAvailable = false
                let openClick = cc.callFunc(() => {
                    this.isAvailable = true
                }, this)
                this.node.runAction(cc.sequence(this.setJumpDoubleAction(0.3, 150), openClick))
            } else if (lastClick - firstClick >= 600) {
                // single
                this.node.runAction(this.setJumpAction(0.3, 180))
            } else { return }
        } else { return }

    }

    onTouchStart() {
        let curClick = new Date().getTime()
        this.checkClick(this.clickTime, curClick)
        this.clickTime = curClick
    }

    // update(dt) {}
}
